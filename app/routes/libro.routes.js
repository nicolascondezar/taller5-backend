const { EmptyResultError } = require("sequelize/types");

module.exports = app => {
    const libro = require("../controllers/libro.controller.js");
  
    var router = require("express").Router();
  

    router.post("/", libro.create);  
    router.get("/", libro.findAll);
    router.delete("/:id",libro.delete) 
    router.put("/:id", libro.update);

    app.use("/api/libro", router);
  };
  
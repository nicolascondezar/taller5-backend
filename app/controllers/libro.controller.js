const db = require("../models");
const Libro = db.libro;
const Op = db.Sequelize.Op;

exports.create = (req, res) => {

  // Create a Tutorial
  const libro = {
    autor: req.body.autor,
    titulo: req.body.titulo,
    ano : req.body.ano
  };

  // Save Tutorial in the database
  Libro.create(libro)
    .then(data => {
      res.status(201).send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Error al registra libro."
      });
    });
};

exports.findAll = (req, res) => {

  Libro.findAll()
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Error."
      });
    });
};

exports.delete = (req, res) => {
    const id = req.params.id;

    Libro.destroy({
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "Libro se elimino correctamente"
          });
        } else {
          res.send({
            message: `No se puede eliminar libro id=${id}. No se encuentra`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "No se podra eliminar el libro id=" + id
        });
      });
  };

  exports.update = (req, res) => {
    const id = req.params.id;

    Libro.update(req.body, {
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "Libro actualizado."
          });
        } else {
          res.send({
            message: `No se puede actualizar lbro id=${id}. No se encuentra!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Error actualizar libro id=" + id
        });
      });
  };